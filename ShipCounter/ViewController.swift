//
//  ViewController.swift
//  ShipCounter
//
//  Created by Ivan Developer on 21.01.16.
//  Copyright © 2016 Ivan Developer. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // Variables block
    @IBOutlet weak var backgroundImageOutlet: UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.setBackgroundImage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Based on the month set background
    func setBackgroundImage(){
        let date = NSDate()
        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components(.Month, fromDate: date)
        
        switch components.month{
        case 1, 2, 12:
            self.backgroundImageOutlet.image = UIImage(named: "backgroundWinterIphone")
        case 3, 4, 5:
            self.backgroundImageOutlet.image = UIImage(named: "backgroundSpringIphone")
        case 6, 7, 8:
            self.backgroundImageOutlet.image = UIImage(named: "backgroundSummerIphone")
        case 9, 10, 11:
            self.backgroundImageOutlet.image = UIImage(named: "backgroundFallIphone")
        default:
            self.backgroundImageOutlet.image = UIImage(named: "backgroundWinterIphone")
        }
    }
}

